﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBooking.DataAccess
{
    public interface IEBookingDbRepositories
    {
        ICategoryRepository CategoryRep { get; }
        ICoverTypeRepository CoverTypeRep { get; }
        IProductRepository ProductRep { get; }
        ICompanyRepository CompanyRep { get; }
        IShoppingCartRepository ShoppingCartRep { get; }    
        IApplicationUserRepostiory ApplicationUserRep { get; }  
        IOrderDetailRepository OrderDetailRep { get; }
        IOrderHeaderRepository OrderHeaderRep { get; }


        void Save();
    }
}
