﻿using Microsoft.AspNetCore.Mvc;
using EBooking.DataAccess;
using EBooking.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EBooking.Controllers
{
    [Area("Admin")]
    public class CompanyController : Controller
    {
        private readonly IEBookingDbRepositories _db;
        public CompanyController(IEBookingDbRepositories db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }
        //Get
        public ActionResult Upsert(int? id)
        {
            Company company = new();
            if(id ==0 || id== null)
            {
                return View(company);
            }
            else
            {
                company = _db.CompanyRep.GetFirstOrDefault(u => u.Id == id);
                return View(company);
            }
        }
        [HttpPost]
        public IActionResult Upsert(Company obj)
        {
            if (ModelState.IsValid)
            {
                if (obj.Id == 0)
                {
                    _db.CompanyRep.Add(obj);
                }
                else
                {
                    _db.CompanyRep.Update(obj);
                }
                _db.Save();
                TempData["success"] = "Company created successfully";
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        #region "API Calls"
        [HttpGet]
        public ActionResult GetAll()
        {
            var allCompanies = _db.CompanyRep.GetAll();
            return Json(new {data=allCompanies});    
        }

        [HttpDelete]
        public IActionResult Delete(int? id)
        {
            var company = _db.CompanyRep.GetFirstOrDefault(u=> u.Id== id);
            if(company == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }            _db.CompanyRep.Remove(company);
            _db.Save();
            return Json(new { success = true, message = "Company deleted successfully" });
            
        }
        #endregion  
    }
}
